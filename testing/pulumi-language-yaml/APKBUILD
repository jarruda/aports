# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi-language-yaml
pkgver=1.2.1
pkgrel=0
pkgdesc="Infrastructure as Code SDK (YAML language provider)"
url="https://pulumi.com/"
# blocked by pulumi
arch="x86_64 aarch64"
license="Apache-2.0"
depends="pulumi"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi-yaml/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pulumi-yaml-$pkgver"
# Requires PULUMI_ACCESS_TOKEN for tests to be run
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -v \
		-ldflags "-X github.com/pulumi/pulumi-yaml/pkg/version.Version=v$pkgver" \
		-o $pkgname \
		./cmd/pulumi-language-yaml/
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
327edb9e2d6efa6cf46cdd515b189134099d0f9c90aee45832da431d9edc7688cd5313500741ca1bd889a78e99cd450292004bc6498886431a52e06146900077  pulumi-language-yaml-1.2.1.tar.gz
"
