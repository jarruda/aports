# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php8-pecl-protobuf
_extname=protobuf
pkgver=3.24.0
pkgrel=0
pkgdesc="PHP 8.0 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data - PECL"
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
_phpv=8
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Test suite is not a part of pecl release.
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
1857be3cc75fd2e58a8b14025887cdc7d3442dfdc876d1bf31c69e064d93e622e759880a557e58c1d3e9c9da4c0ee6ba286b5d3c4fd01b91860cd3322a0502f5  php-pecl-protobuf-3.24.0.tgz
"
