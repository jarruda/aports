# Contributor: rubicon <rubicon@mailo.com>
# Maintainer: rubicon <rubicon@mailo.com>
pkgname=gleam
pkgver=0.30.5
pkgrel=0
pkgdesc="Statically-typed language that compiles to Erlang and JS"
url="https://gleam.run/"
# s390x, riscv64, ppc64le: ring
# armhf: error: Undefined temporary symbol .LBB88_2
arch="all !armhf !s390x !riscv64 !ppc64le"
license="Apache-2.0"
depends="erlang-dev"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/gleam-lang/gleam/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dvm755 target/release/gleam -t "$pkgdir"/usr/bin/
}

sha512sums="
54bc24fcfc0e4f302d2b716f5491048b06aee22e077770241c593c94f81b84617f35f7c995a8947c1d675469ffc1eaea85fed04ad3fbf49140f67609845e7b27  gleam-0.30.5.tar.gz
"
