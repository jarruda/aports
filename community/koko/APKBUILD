# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=koko
pkgver=23.01.0
pkgrel=2
_geonames_pkgver=2020.06.25
pkgdesc="Image gallery application for Plasma Mobile"
url="https://invent.kde.org/graphics/koko"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
license="LGPL-2.0-or-later AND LGPL-2.1-only AND LGPL-3.0-only AND LicenseRef-KDE-Accepted-GPL"
depends="
	kdeclarative
	kirigami2
	kquickimageeditor
	purpose
	qt5-qtbase-sqlite
	"
makedepends="
	exiv2-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdeclarative-dev
	kfilemetadata-dev
	kguiaddons-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kquickimageeditor-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/graphics/koko.git"
source="https://download.kde.org/stable/plasma-mobile/$pkgver/koko-$pkgver.tar.xz
	geonames-$_geonames_pkgver.tar.gz::https://github.com/pmsourcedump/geonames/archive/$_geonames_pkgver/geonames-$_geonames_pkgver.tar.gz
	"
options="!check" # Broken tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

prepare() {
	default_prepare

	mv "$srcdir"/geonames-$_geonames_pkgver/cities1000.zip src/
	mv "$srcdir"/geonames-$_geonames_pkgver/admin1CodesASCII.txt src/
	mv "$srcdir"/geonames-$_geonames_pkgver/admin2Codes.txt src/
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
398e22dfeadb8720d616b6b34773560bc3cfa080deab655b4d0881edda8d95857e4962794a5a02ddf128b654547225ceb600eae8c40488949de2865850d437fe  koko-23.01.0.tar.xz
07485983a5ce0f03f1e12e64c280abe01e81beaa22b2dd43bc1b0e7632298acbfb83f09ef8c01a0915481c8e918a430b97d68f1ed5d43f76506798245345bc14  geonames-2020.06.25.tar.gz
"
