# Contributor:
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-hypothesis
pkgver=6.82.6
pkgrel=0
pkgdesc="Advanced property-based (QuickCheck-like) testing for Python"
options="!check"
url="https://hypothesis.works/"
arch="noarch"
license="MPL-2.0"
depends="py3-attrs py3-sortedcontainers"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-coverage py3-tz py3-numpy py3-dateutil py3-pexpect"
subpackages="$pkgname-pyc"
source="py-hypothesis-$pkgver.tar.gz::https://github.com/HypothesisWorks/hypothesis-python/archive/hypothesis-python-$pkgver.tar.gz
	"
builddir="$srcdir/hypothesis-hypothesis-python-$pkgver/hypothesis-python"

replaces="py-hypothesis" # Backwards compatibility
provides="py-hypothesis=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	# In python3 mock is actually unittest.mock
	sed -e 's/from mock/from unittest.mock/' -i tests/cover/test_regressions.py
	sed -e 's/from mock/from unittest.mock/' -i tests/cover/test_reflection.py

	rm -rf tests/lark tests/dpcontracts tests/pandas
	PYTHONPATH="$PWD/build/lib" pytest-3 -v -k "not test_healthcheck_traceback_is_hidden"
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
507656462a88a436a1b250cd348770f8e3f359cd9988ea9c97dd98e6f82bf05a827b6cff394da6b7102ddf31b17443d25df31ee2efaf814f08a174617d3e59c0  py-hypothesis-6.82.6.tar.gz
"
