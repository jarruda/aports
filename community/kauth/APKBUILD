# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kauth
pkgver=5.109.0
pkgrel=0
pkgdesc="Abstraction to system policy and authentication features"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcoreaddons-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kauth.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kauth-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kauth.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# KAuthHelperTest hangs
	ctest --test-dir build --output-on-failure -E '(KAuthHelperTest)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a588feb84a9f19a224daf856c63f94a52ae382390230f7264a486645763f4cec09c299cd7301b3a7ec050080982aeb4a60c02446ae35015391c51a8048e744d1  kauth-5.109.0.tar.xz
"
