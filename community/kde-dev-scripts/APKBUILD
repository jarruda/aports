# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kde-dev-scripts
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf !s390x"
url="https://kde.org/applications/development/"
pkgdesc="Scripts and setting files useful during development of KDE software"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kde-dev-scripts.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-scripts-$pkgver.tar.xz"
subpackages="$pkgname-doc"
options="!check" # No code to test

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5c9a28eb5b25e0fef0a2b4a30094fa142f592590d092f73ef6cb8283859a79723ebd1f9c742f14fe23311b43590d817f40c744c08fa3f449ef74422fdc31d62e  kde-dev-scripts-23.04.3.tar.xz
"
