# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kpublictransport
pkgver=23.04.3
pkgrel=1
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/libraries/kpublictransport"
pkgdesc="Library to assist with accessing public transport timetables and other data"
license="BSD-3-Clause AND LGPL-2.0-or-later AND MIT"
depends_dev="
	ki18n-dev
	networkmanager-qt-dev
	protobuf-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/libraries/kpublictransport.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpublictransport-$pkgver.tar.xz"
options="!check" # Broken for now
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
41597bf9e7c3366575a9a6698cace1f127f8c69094de906d0ab662cc563b0a440ae89c074909083d82c8d94e1cd5f2442470f45369663252b5cf6dd8f6ae7e47  kpublictransport-23.04.3.tar.xz
"
