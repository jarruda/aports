# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=verilator
pkgver=5.014
pkgrel=0
pkgdesc="Convert Verilog and SystemVerilog to C++ or SystemC"
url="https://verilator.org"
arch="x86 x86_64" # limited by systemc
license="LGPL-3.0-only"
depends="perl"
makedepends="autoconf automake python3 systemc-dev flex flex-dev bison lsb-release help2man"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/verilator/verilator/archive/refs/tags/v$pkgver/verilator-$pkgver.tar.gz"

prepare() {
	default_prepare
	autoconf
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
963c15290089fd59870bf15903e71aa29fcc10eb67b3d3f1ae0128e8a29e5dc086c6f3a8d429c24718f54c3cbc3d3bcaa137bd2fa3165a0190c591ed5fc06f37  verilator-5.014.tar.gz
"
