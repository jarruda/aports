# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-zeroconf
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Network Monitor for DNS-SD services (Zeroconf)"
license="GPL-2.0-or-later AND LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdnssd-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/kio-zeroconf.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-zeroconf-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
83c401b0d5cc32088990635e805c9881bd234b5919eab973f26021841df0d3e06595c68d2b9a37dcbaa4158349a2540650b7897ef5209d7de54fa5913627df8a  kio-zeroconf-23.04.3.tar.xz
"
