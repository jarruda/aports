# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-pypdf
pkgver=3.13.0
pkgrel=0
pkgdesc="Pure-Python library built as a PDF toolkit"
url="https://github.com/py-pdf/pypdf"
arch="noarch"
license="BSD-3-Clause"
options="!check" # issues with reading pdf files from test dirs
makedepends="py3-gpep517 py3-installer py3-flit-core"
checkdepends="py3-pillow py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/py-pdf/pypdf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/pypdf-$pkgver"

provides="py3-pypdf2=$pkgver-r$pkgrel"
replaces="py3-pypdf2"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
6259b242aaba8e0b63b988749850aa2138b802eaf52d146793635ff723956f19b1989c3c76278ed4240acbc88f8dd4b7e64fcf777508e98dee81093d794b2945  py3-pypdf-3.13.0.tar.gz
"
