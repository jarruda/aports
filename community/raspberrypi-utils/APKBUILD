# Contributor: macmpi <spam@ipik.org>
# Maintainer: macmpi <spam@ipik.org>
pkgname=raspberrypi-utils
pkgver=0.20230809
pkgrel=0
_commit="94b89a69cdf3d10cbe8ee7571584950e5df768f5"
pkgdesc="Collection of Raspberry Pi utilities (scripts and simple applications)"
url="https://github.com/raspberrypi/utils"
arch="armhf armv7 aarch64"
license="BSD-3-Clause"
makedepends="cmake samurai dtc-dev"
source="$pkgname-$_commit.tar.gz::https://github.com/raspberrypi/utils/archive/$_commit.tar.gz"
builddir="$srcdir/utils-$_commit"
# does not have any tests
options="!check"
subpackages="
	$pkgname-vclog
	$pkgname-raspinfo::noarch
	$pkgname-dtmerge
	$pkgname-dtmerge-doc
	$pkgname-ovmerge::noarch
	$pkgname-overlaycheck::noarch
	$pkgname-otpset::noarch
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build build
}

package() {
	depends="
		$pkgname-vclog=$pkgver-r$pkgrel
		$pkgname-raspinfo=$pkgver-r$pkgrel
		$pkgname-dtmerge=$pkgver-r$pkgrel
		$pkgname-ovmerge=$pkgver-r$pkgrel
		$pkgname-overlaycheck=$pkgver-r$pkgrel
		$pkgname-otpset=$pkgver-r$pkgrel
		"
	DESTDIR="$pkgdir" cmake --install build
}

vclog() {
	pkgdesc="$pkgdesc (vclog tool)"
	depends=""

	amove usr/bin/vclog
}

raspinfo() {
	pkgdesc="$pkgdesc (raspinfo bash tool)"
	depends="
		$pkgname-vclog=$pkgver-r$pkgrel
		bash
		raspberrypi-userland
		sudo-virt
		usbutils
		"
	# missing https://github.com/RPi-Distro/raspi-gpio
	# missing https://github.com/raspberrypi/rpi-eeprom

	amove usr/bin/raspinfo
}

dtmerge() {
	pkgdesc="$pkgdesc (dtmerge tool)"
	depends=""

	amove usr/bin/dtmerge
}

ovmerge() {
	pkgdesc="$pkgdesc (ovmerge tool)"
	depends="perl"

	amove usr/bin/ovmerge
}

overlaycheck() {
	pkgdesc="$pkgdesc (overlaycheck tool)"
	depends="
		$pkgname-dtmerge=$pkgver-r$pkgrel
		$pkgname-ovmerge=$pkgver-r$pkgrel
		dtc
		perl
		"

	amove usr/bin/overlaycheck
	amove usr/bin/overlaycheck_exclusions.txt
}

otpset() {
	pkgdesc="$pkgdesc (otpset tool)"
	depends="python3 raspberrypi-userland"

	amove usr/bin/otpset
}

sha512sums="
a17ee2e654e1a93f839591aaf5a2d1650efe18e77b676fd3a9f47f05525ed64d3b0637f3b341a28247b424c6bf0a046704e161b8b7376968117a7e9625ab20ae  raspberrypi-utils-94b89a69cdf3d10cbe8ee7571584950e5df768f5.tar.gz
"
