# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=janet
pkgver=1.30.0
pkgrel=0
pkgdesc="Dynamic Lisp dialect and bytecode VM"
url="https://janet-lang.org/"
license="MIT"
arch="all !riscv64" # unsupported
makedepends="meson"
subpackages="$pkgname-static $pkgname-doc $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/janet-lang/janet/archive/v$pkgver.tar.gz"

# secfixes:
#   1.22.0-r0:
#     - CVE-2022-30763

case "$CARCH" in
x86_64) ;;
# FIXME
*) options="$options !check" ;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Depoll=true \
		-Dos_name="alpine" \
		. output
	meson compile -C output
}

check() {
	meson test --print-errorlogs --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -dm755 "$pkgdir"/usr/share/doc/janet
	cp -a examples "$pkgdir"/usr/share/doc/janet
}

sha512sums="
e7e893dda0c928211f8ee2e7d3f155fbe83a5a714a4e32d3c5a6d4511b9dbd600c2a6cef7bc366a04e25d2f4b0822609ccc8f1de0bf12e99c6f45f26add0347f  janet-1.30.0.tar.gz
"
